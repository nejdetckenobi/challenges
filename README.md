# About this repo

Please `cd` to each task you want to see.

To run tests (after `cd`ing to the task's directory), simply run

`pytest --cov="." --cov-report term-missing tests`
