from string import ascii_lowercase


def find_anagrams_of_word_from_list(word, wordlist):
    if not isinstance(wordlist, tuple) and not isinstance(wordlist, list):
        raise TypeError(
            'wordlist should be a list of strings or tuple of strings')
    if not isinstance(word, str):
        raise TypeError('word should be str')
    anagrams_found = []
    word = ''.join([c for c in word.lower() if c in ascii_lowercase])
    rearranged_word = ''.join(sorted(word))
    for candidate in wordlist:
        list_candidate = [c for c in candidate.lower() if c in ascii_lowercase]
        if ''.join(sorted(list_candidate)) == rearranged_word:
            anagrams_found.append(candidate)
    return anagrams_found
