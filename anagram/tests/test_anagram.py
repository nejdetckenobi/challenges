import os
import sys
import unittest

# If not, path issues will occur while running 'pytest'
defpath = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, defpath + '/../src')

from anagram import find_anagrams_of_word_from_list


TEST_DATA = {
    'The Best Things in Life are Free': 'Nail-biting refreshes the feet',
    'The End of the World is Nigh': 'Down this hole frightened',
    'The Meaning of Life': 'The fine game of nil',
    'Public relations': 'Crap built on lies.',
    'Male chauvinism': 'I\'m such a vile man',
    'Mother-in-Law': 'Woman Hitler',
}


class TestAnagramFinder(unittest.TestCase):
    def test_anagram_finder_with_no_tuple_or_list_wordlist(self):
        wordlist = object()
        with self.assertRaises(TypeError, msg='TypeError expected.'):
            find_anagrams_of_word_from_list('WORD', wordlist)

    def test_anagram_finder_with_non_string_word(self):
        word = object()
        with self.assertRaises(TypeError, msg='TypeError expected.'):
            find_anagrams_of_word_from_list(word, ['qwe'])

    def test_anagram_finder_working(self):
        for word, anagram in TEST_DATA.items():
            wordlist = list(TEST_DATA.values())
            result = find_anagrams_of_word_from_list(word, wordlist)
            self.assertListEqual(result, [anagram],
                                 msg='Anagram calculation is wrong')
