# Lottery Draw Date Calculation Task

You can use the `get_next_lottery_draw_date` to get next draw date.

To run tests, simply run

`pytest --cov="." --cov-report term-missing tests`
