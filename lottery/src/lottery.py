from datetime import datetime, timedelta

LOTTERY_DRAW_DAYS_OF_WEEK = (2, 5)
LOTTERY_DRAW_HOUR = 20


def get_next_lottery_draw_date(datetime_arg=None):
    if datetime_arg is None:
        datetime_arg = datetime.now()
    elif not isinstance(datetime_arg, datetime):
        raise TypeError('"datetime_arg" argument should be a datetime object')

    if datetime_arg.microsecond > 0:
        datetime_arg += timedelta(seconds=1)
        datetime_arg = datetime_arg.replace(microsecond=0)

    if datetime_arg.second > 0:
        datetime_arg += timedelta(minutes=1)
        datetime_arg = datetime_arg.replace(second=0)

    if datetime_arg.minute > 0:
        datetime_arg += timedelta(hours=1)
        datetime_arg = datetime_arg.replace(minute=0)

    while datetime_arg.hour != LOTTERY_DRAW_HOUR:
        datetime_arg += timedelta(hours=1)

    while datetime_arg.weekday() not in LOTTERY_DRAW_DAYS_OF_WEEK:
        datetime_arg += timedelta(days=1)

    return datetime_arg
