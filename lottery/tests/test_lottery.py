import os
import sys
import unittest
from unittest.mock import patch
from datetime import datetime

# If not, path issues will occur while running 'pytest'
defpath = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, defpath + '/../src')

from lottery import get_next_lottery_draw_date


TEST_DATES = (
    datetime(year=2018, month=6, day=23, hour=20, minute=0, second=0, microsecond=0),  # Exact draw time (saturday)
    datetime(year=2018, month=6, day=23, hour=20, minute=0, second=0, microsecond=1),  # A microsecond late
    datetime(year=2018, month=6, day=23, hour=20, minute=0, second=1, microsecond=0),  # A second late
    datetime(year=2018, month=6, day=23, hour=20, minute=1, second=0, microsecond=0),  # A minute late
    datetime(year=2018, month=6, day=23, hour=21, minute=0, second=0, microsecond=0),  # An hour late
    datetime(year=2018, month=6, day=24, hour=20, minute=0, second=0, microsecond=0),  # A day late
    datetime(year=2018, month=6, day=27, hour=20, minute=0, second=0, microsecond=0),  # Exact draw time (wednesday)
    datetime(year=2018, month=6, day=27, hour=20, minute=0, second=0, microsecond=1),  # A microsecond late
    datetime(year=2018, month=6, day=27, hour=20, minute=0, second=1, microsecond=0),  # A second late
    datetime(year=2018, month=6, day=27, hour=20, minute=1, second=0, microsecond=0),  # A minute late
    datetime(year=2018, month=6, day=27, hour=21, minute=0, second=0, microsecond=0),  # An hour late
    datetime(year=2018, month=6, day=28, hour=20, minute=0, second=0, microsecond=0),  # A day late
)

VALIDATION_DATA = (
    datetime(year=2018, month=6, day=23, hour=20, minute=0, second=0, microsecond=0),
    datetime(year=2018, month=6, day=27, hour=20, minute=0, second=0, microsecond=0),
    datetime(year=2018, month=6, day=27, hour=20, minute=0, second=0, microsecond=0),
    datetime(year=2018, month=6, day=27, hour=20, minute=0, second=0, microsecond=0),
    datetime(year=2018, month=6, day=27, hour=20, minute=0, second=0, microsecond=0),
    datetime(year=2018, month=6, day=27, hour=20, minute=0, second=0, microsecond=0),
    datetime(year=2018, month=6, day=27, hour=20, minute=0, second=0, microsecond=0),

    datetime(year=2018, month=6, day=30, hour=20, minute=0, second=0, microsecond=0),
    datetime(year=2018, month=6, day=30, hour=20, minute=0, second=0, microsecond=0),
    datetime(year=2018, month=6, day=30, hour=20, minute=0, second=0, microsecond=0),
    datetime(year=2018, month=6, day=30, hour=20, minute=0, second=0, microsecond=0),
    datetime(year=2018, month=6, day=30, hour=20, minute=0, second=0, microsecond=0),
)


class TestLottery(unittest.TestCase):
    def test_is_working_correct_for_given_datetime(self):
        for index, dt in enumerate(TEST_DATES):
            result = get_next_lottery_draw_date(dt)
            print(result)
            self.assertEqual(result, VALIDATION_DATA[index],
                             msg='calculation is wrong')

    def test_for_non_datetime_argument(self):
        arg = object()
        with self.assertRaises(TypeError, msg='TypeError expected'):
            get_next_lottery_draw_date(arg)

    @patch('lottery.datetime')
    def test_with_no_argument(self, datetime_mock):
        datetime_mock.now.return_value = TEST_DATES[0]
        result = get_next_lottery_draw_date()
        print(result)
        self.assertEqual(result, VALIDATION_DATA[0],
                         msg='Result is wrong')
