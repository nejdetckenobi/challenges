# LRU Cache Task

You may want to run `pip3 install redis` first.

Because the code is close to production ready.

And then, please run (and open in your text editor to see explanations)

`small_script.py`.

Inline comments are available.

To run tests, simply run

`pytest --cov="." --cov-report term-missing tests`
