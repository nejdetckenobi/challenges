from threading import RLock
from redis import Redis  # Because you asked a "product ready" code, I used it.
import json  # for creating SHA hashable objects.
import pickle
from hashlib import sha1  # For unique and fixed-length key generation


class lru_cached(object):
    """
    LRU Cache decorator.
    CAUTION! All args and kwargs must be hashable.
    """
    def __init__(self, maxsize, db=0, key_generator=sha1):
        super(lru_cached, self).__init__()
        self.maxsize = maxsize
        self.storage = Redis(db=db)
        self.key_generator = key_generator
        self.lock = RLock()

    def create_arg_key(self, fname, args, kwargs):
        kwarg_tuple = sorted(kwargs.items(), key=lambda i: i[0])
        arg_data = json.dumps((args, kwarg_tuple))
        key = "{}-{}".format(
            fname, self.key_generator(arg_data.encode()).hexdigest())
        return key

    def renew(self, fname, hstring):
        # '-' is a good character for separating the name and the hash value
        # Because neither function name nor hash value can contain it.
        # Used a prefix, just in case.
        func_key = 'function-' + str(fname)
        # This will move the old (but called) key to the edge.
        self.storage.lrem(func_key, hstring)
        self.storage.lpush(func_key, hstring)

    def get(self, fname, hstring):
        with self.lock:
            self.renew(fname, hstring)
            raw_data = self.storage.get(hstring)
            if not raw_data:
                return None
            data = pickle.loads(raw_data)
        return data

    def set(self, fname, hstring, data):
        # RLocks are useful because the pipeline mechanism was failing somehow.
        with self.lock:
            func_key = 'function-{}'.format(fname)
            self.renew(fname, hstring)
            keys_to_delete = self.storage.lrange(func_key, self.maxsize, -1)
            self.storage.ltrim(func_key, 0, self.maxsize - 1)
            for key in keys_to_delete:
                self.storage.delete(key)
            raw_data = pickle.dumps(data)
            self.storage.set(hstring, raw_data)

    def __call__(self, f):
        def wrapped(*args, **kwargs):
            arg_key = self.create_arg_key(f.__name__, args, kwargs)
            # Try to get the cached value.
            cached_data = self.get(f.__name__, arg_key)
            # Oh good, it was cached.
            if cached_data is not None:
                return cached_data
            # It wasn't cached.
            # I need to call the function...
            result = f(*args, **kwargs)
            # ...and cache the result
            self.set(f.__name__, arg_key, result)
            return result
        return wrapped
