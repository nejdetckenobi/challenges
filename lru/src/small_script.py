import redis  # Because you asked a "product ready" code, I used it.
from cache import lru_cached  # The cache mechanism implemented by me.

# I know that redis already supports a LRU cache mechanism but
#  in order to show you that I know how to implement the algorithm,
#  I didn't use it. Otherwise, there was an extension for that.

r = redis.Redis(db=0)  # using the same db value to clear cached items.

CACHE_SIZE = 2


def clear_all_cache():  # For clearing Redis.
    global r
    for k in r.keys():
        r.delete(k)


# An annoying function that prints every time it is called.
@lru_cached(maxsize=CACHE_SIZE)
def f(*args, **kwargs):
    print('I am called with', args, kwargs)
    return args, kwargs


if __name__ == '__main__':
    clear_all_cache()  # clean start

    # Is it really caching?
    a, k = f(1, 2, '3', value='LoL')
    print('Results are', a, 'and', k)
    a, k = f(1, 2, '3', value='LoL')
    print('Results are', a, 'and', k)

    # What if I cache one more thing then call again?
    a, k = f(4, '5', 6.0, value='I have flu.')
    print('Results are', a, 'and', k)
    a, k = f(4, '5', 6.0, value='I have flu.')
    print('Results are', a, 'and', k)

    # I'll call the old one again to see if it's still cached.
    a, k = f(1, 2, '3', value='LoL')
    print('Results are', a, 'and', k)

    # Now I cache one more thing and call the second one again.
    # Because of the LRU mechanism, That should call the function.
    a, k = f('7', '8', 9, taramali='fuze')
    print('Results are', a, 'and', k)
    a, k = f(4, '5', 6.0, value='I have flu.')
    print('Results are', a, 'and', k)

    clear_all_cache()  # Just disable this line to see values in redis.
