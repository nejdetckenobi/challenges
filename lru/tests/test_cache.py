import os
import sys
import unittest
from unittest.mock import patch, MagicMock

# If not, path issues will occur while running 'pytest'
defpath = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, defpath + '/../src')

from cache import lru_cached


TEST_ARGS = (1, '2', 3.0)
TEST_KWARGS = {'my': 'little', 'little': 'pony'}


def f(*args, **kwargs):
    return args, kwargs


class TestF(unittest.TestCase):
    def test_function(self):
        result = f(*TEST_ARGS, **TEST_KWARGS)
        self.assertTupleEqual(result[0], TEST_ARGS,
                              msg='Positional arguments are wrong.')
        self.assertDictEqual(result[1], TEST_KWARGS,
                             msg='Keyword arguments are wrong')


class TestCache(unittest.TestCase):
    @patch('cache.Redis')
    @patch('cache.RLock')
    @patch('cache.sha1')
    def setUp(self, sha1, RLock, Redis):
        self.cache_obj = lru_cached(maxsize=3, key_generator=MagicMock())
        self.cache_obj.key_generator.return_value.hexdigest.return_value = \
            '1234'
        self.redis = Redis
        self.RLock = RLock

    def test_init(self):
        self.assertEqual(self.cache_obj.maxsize, 3,
                         msg='maxsize is wrong')
        self.assertIsInstance(self.cache_obj.storage, MagicMock,
                              msg='storage is wrong')
        self.assertIsInstance(self.cache_obj.key_generator, MagicMock,
                              msg='Key generator is wrong')
        self.assertIsInstance(self.cache_obj.lock, MagicMock,
                              msg='Lock object is wrong')

    @patch('cache.sha1')
    def test_create_arg_key(self, sha1):
        sha1.return_value.hexdigest.return_value = '1234'
        result = self.cache_obj.create_arg_key(
            'somefunctionname',
            ('pos1', 'pos2', 'pos3'),
            {'k1': 'v1', 'k2': 'v2'}
        )
        self.assertEqual(result, 'somefunctionname-1234', msg=None)

    def test_renew(self):
        self.cache_obj.renew('hello', '1234')

    def test_get_data_not_found(self):
        self.cache_obj.storage.get.return_value = None
        result = self.cache_obj.get('somefunctionname', 'hash1234')
        self.assertIsNone(result)

    @patch('cache.pickle.loads', return_value='#SOMESTRING#')
    def test_get_data_found(self, loads):
        self.cache_obj.storage.get.return_value = object()
        result = self.cache_obj.get('somefunctionname', 'hash1234')
        self.assertEqual(result, '#SOMESTRING#')

    @patch('cache.pickle.dumps', return_value=b'#SOMEBYTES#')
    def test_set_data(self, dumps):
        self.cache_obj.storage.lrange.return_value = ['1', '2', '3']
        self.cache_obj.set('somefunctionname', 'hash1234', b'raw_data')

    def test_decorator_cached_data_found(self):
        self.cache_obj.get = MagicMock(return_value='#SOMESTRING#')
        decorated_func = self.cache_obj(lambda x: x)
        result = decorated_func(1, 2, 3, k='v', k2='v2')
        print(result)
        self.assertEqual(result, '#SOMESTRING#',
                         msg='Cached result is wrong')

    def test_decorator_cached_data_not_found(self):
        self.cache_obj.get = MagicMock(return_value=None)
        self.cache_obj.set = MagicMock()
        decorated_func = self.cache_obj(
            lambda *args, **kwargs: 'DIFFERENT_STRING')
        result = decorated_func(1, 2, 3, k='v', k2='v2')
        print(result)
        self.assertEqual(result, 'DIFFERENT_STRING',
                         msg='Calculated response is wrong')
